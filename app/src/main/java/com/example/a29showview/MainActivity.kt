package com.example.a29showview

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.PopupWindow
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_popupwindow.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var popupWindow: PopupWindow
    private lateinit var rootView: View
    private var isFloationActionButtonOpen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title = "ShowView"
        rootView = LayoutInflater.from(this).inflate(R.layout.activity_main, null)

        setupPopupWindow()
        setupFloatingActionButton()
    }

    private fun setupPopupWindow() {
        // init
        popupWindow = PopupWindow(this)
    }

    private fun setupFloatingActionButton() {
        val floatingActionButton1: FloatingActionButton = findViewById(R.id.layout_main_floatingActionButton1)
        val floatingActionButton2: FloatingActionButton = findViewById(R.id.layout_main_floatingActionButton2)
        val floatingActionButton3: FloatingActionButton = findViewById(R.id.layout_main_floatingActionButton3)

        // hide two button at the beginning
        floatingActionButton2.visibility = View.INVISIBLE
        floatingActionButton3.visibility = View.INVISIBLE

        // init animations
        val showLayoutAnimation = AnimationUtils.loadAnimation(this, R.anim.show_layout)
        val hideLayoutAnimation = AnimationUtils.loadAnimation(this, R.anim.hide_layout)
        val showButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.show_button)
        val hideButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.hide_button)

        // setup listener
        layout_main_showButton.setOnClickListener(showButtonClickHandler)

        // open/close FloatingActionButton
        floatingActionButton1.setOnClickListener{
            if(isFloationActionButtonOpen) {
                floatingActionButton2.startAnimation(hideLayoutAnimation)
                floatingActionButton3.startAnimation(hideLayoutAnimation)
                floatingActionButton1.startAnimation(hideButtonAnimation)

            } else {
                floatingActionButton2.startAnimation(showLayoutAnimation)
                floatingActionButton3.startAnimation(showLayoutAnimation)
                floatingActionButton1.startAnimation(showButtonAnimation)
            }

            isFloationActionButtonOpen = !isFloationActionButtonOpen

        }

        floatingActionButton2.setOnClickListener{
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("tel:0912")))
        }

        floatingActionButton3.setOnClickListener{
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("sms:0912")))

        }
    }

    private var showButtonClickHandler = View.OnClickListener {
        showPopupWindow()
    }

    private fun showPopupWindow() {
        val popupView = LayoutInflater.from(this).inflate(R.layout.layout_popupwindow, null)

        // set click listener for ok button
        popupView.layout_popupwindow_confirmButton.setOnClickListener{
            popupWindow.dismiss()
        }

        popupWindow.contentView = popupView
        popupWindow.width = ViewGroup.LayoutParams.MATCH_PARENT
        popupWindow.height = ViewGroup.LayoutParams.WRAP_CONTENT

        // disappear popupWindow if touch outside of it
        popupWindow.isOutsideTouchable = true

        // show popWindow
        popupWindow.showAsDropDown(layout_main_showButton)
    }

}